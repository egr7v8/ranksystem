<?php

namespace ru\universalcrew\ranksystem\experience;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\Player;
use ru\universalcrew\ranksystem\Home;

class Experience
{

    /**
     * @var Home
     */
    private $home;

    /**
     * Experience constructor.
     * @param Home $home
     */
    function __construct(Home $home)
    {
        $this->home = $home;
    }

    function addExp(Player $player, int $exp) : void
    {
        $nickname = $player->getName();
        $this->getHome()->getProvider()->addExperience($nickname, $exp);
        $exp = $this->getHome()->getProvider()->getExperiencePlayer($nickname);
        $level = $this->getHome()->getProvider()->getLevelPlayer($nickname);
        $rank = $this->getHome()->getProvider()->getRankPlayer($nickname);
        if ($this->getHome()->getProvider()->isNextLevel($rank, $level)){
            $levelexp = $this->getHome()->getProvider()->getLevelExp($rank, $level + 1);
            if ($exp >= $levelexp){
                $this->getHome()->getProvider()->setLevelPlayer($nickname, $level + 1);
            }
        } else {
            if ($this->getHome()->getProvider()->isNextRank($rank)){
                $this->getHome()->getProvider()->setRankPlayer($nickname, $rank + 1);
                $this->getHome()->getProvider()->setLevelPlayer($nickname, 0);
                $this->getHome()->getProvider()->setExperience($nickname, 0);
                $groupName = $this->getHome()->getProvider()->getGiftGroup($rank);
                $group = $this->getHome()->getPurePerms()->getGroup($groupName);
                $this->getHome()->getPurePerms()->setGroup($player, $group);
                $money = $this->getHome()->getProvider()->getGiftMoney($rank);
                $this->getHome()->getEconomy()->addMoney($player, $money);
                $text = $this->getHome()->getProvider()->getMessage("new_rank");
                $player->addTitle($text);
            }
        }
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }
}