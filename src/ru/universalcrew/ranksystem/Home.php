<?php

namespace ru\universalcrew\ranksystem;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use _64FF00\PurePerms\PurePerms;
use jojoe77777\FormAPI\FormAPI;
use onebone\economyapi\EconomyAPI;
use pocketmine\plugin\PluginBase;
use ru\universalcrew\ranksystem\commands\Rank;
use ru\universalcrew\ranksystem\commands\SetRank;
use ru\universalcrew\ranksystem\experience\Experience;
use ru\universalcrew\ranksystem\listener\block\BlockBreakEvent;
use ru\universalcrew\ranksystem\listener\block\BlockPlaceEvent;
use ru\universalcrew\ranksystem\listener\player\PlayerCraftItemEvent;
use ru\universalcrew\ranksystem\listener\player\PlayerDeathEvent;
use ru\universalcrew\ranksystem\listener\player\PlayerJoinEvent;
use ru\universalcrew\ranksystem\utils\Forms;
use ru\universalcrew\ranksystem\utils\HotBar;
use ru\universalcrew\ranksystem\utils\Provider;

class Home extends PluginBase
{

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var PurePerms
     */
    private $pureperms;

    /**
     * @var Experience
     */
    private $experience;

    /**
     * @var EconomyAPI
     */
    private $eco;

    /**
     * @var FormAPI
     */
    private $form;

    /**
     * @var Forms
     */
    private $forms;

    public function onEnable() : void
    {
        $this->getLogger()->info($this->getDescription()->getName()." включен.");
        $this->loadListener();
        $this->loadProvider();
        $this->loadCommand();
        $this->loadClass();
        $this->loadPlugins();
    }

    public function onDisable() : void
    {
        $this->getProvider()->save();
        $this->getLogger()->info($this->getDescription()->getName()." выключен.");
    }

    private function loadListener() : void
    {
        $list = [
            new PlayerJoinEvent($this),
            new PlayerDeathEvent($this),
            new BlockBreakEvent($this),
            new BlockPlaceEvent($this),
            new PlayerCraftItemEvent($this)
        ];

        foreach ($list as $listener) {
            $this->getServer()->getPluginManager()->registerEvents($listener, $this);
        }
    }

    private function loadProvider() : void
    {
        $this->provider = new Provider($this);
        new HotBar($this);

    }

    private function loadCommand() : void
    {
        $list = [
            new SetRank($this),
            new Rank($this)
        ];
        foreach ($list as $class) {
            $this->getServer()->getCommandMap()->register($this->getDescription()->getName(), $class);
        }
    }

    private function loadClass() : void
    {
        $this->experience = new Experience($this);
        $this->forms      = new Forms($this);
    }

    private function loadPlugins() : void
    {
        $this->pureperms = $this->getServer()->getPluginManager()->getPlugin("PurePerms");
        $this->eco       = $this->getServer()->getPluginManager()->getPlugin("EconomyAPI");
        $this->form      = $this->getServer()->getPluginManager()->getPlugin("FormAPI");
    }

    /**
     * @return Experience
     */
    public function getExperience() : Experience
    {
        return $this->experience;
    }

    /**
     * @return Provider
     */
    public function getProvider() : Provider
    {
        return $this->provider;
    }

    /**
     * @return PurePerms
     */
    public function getPurePerms() : PurePerms
    {
        return $this->pureperms;
    }

    /**
     * @return EconomyAPI
     */
    public function getEconomy() : EconomyAPI
    {
        return $this->eco;
    }

    /**
     * @return FormAPI
     */
    public function getFormAPI() : FormAPI
    {
        return $this->form;
    }

    /**
     * @return Forms
     */
    public function getForms() : Forms
    {
        return $this->forms;
    }
}
