<?php
namespace ru\universalcrew\ranksystem\utils;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\Player;
use ru\universalcrew\ranksystem\Home;
use ru\universalcrew\ranksystem\tasks\HotBarTask;

class HotBar
{

    /**
     * @var Home
     */
    private $home;

    /**
     * HotBar constructor.
     * @param Home $home
     */
    public function __construct(Home $home)
    {
        $this->home = $home;
        $this->getHome()->getScheduler()->scheduleRepeatingTask(new HotBarTask($this), 20);
    }

    public function sendHotBar()
    {
        $players = $this->getHome()->getServer()->getOnlinePlayers();
        foreach ($players as $player) {
            $text = $this->getHome()->getProvider()->getMessage("hotbar");
            $nickname = $player->getName();
            $rank = $this->getHome()->getProvider()->getRankPlayer($nickname);
            $rankName = $this->getHome()->getProvider()->getNameRank($rank);
            $level = $this->getHome()->getProvider()->getLevelPlayer($nickname);
            $exp = $this->getHome()->getProvider()->getExperiencePlayer($nickname);
            if (!$this->getHome()->getProvider()->isNextRank($rank) && !$this->getHome()->getProvider()->isNextLevel($rank, $level)) {
                $full_exp = $this->getHome()->getProvider()->getLevelExp($rank, $level);
                if ($exp > $full_exp) {
                    $text = $this->getHome()->getProvider()->getMessage("hotbar_max_rank");
                    $text = str_replace("%rank%", $rankName, $text);
                    $player->sendPopup($text);
                }
            } else {
                if ($this->getHome()->getProvider()->isNextLevel($rank, $level)) {
                    $last_exp = $this->getHome()->getProvider()->getLevelExp($rank, $level + 1);
                } else {
                    $last_exp = $this->getHome()->getProvider()->getLevelExp($rank, $level);
                }
                $text = str_replace("%last_exp%", $last_exp, $text);
                $text = str_replace("%rank%", $rankName, $text);
                $text = str_replace("%level%", $level, $text);
                $text = str_replace("%exp%", $exp, $text);
                $player->sendPopup($text);
            }
        }
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }
}
