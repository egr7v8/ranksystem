<?php

namespace ru\universalcrew\ranksystem\utils;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use jojoe77777\FormAPI\SimpleForm;
use pocketmine\Player;
use ru\universalcrew\ranksystem\Home;

class Forms
{

    /**
     * @var Home
     */
    private $home;

    /**
     * HotBar constructor.
     * @param Home $home
     */
    public function __construct(Home $home)
    {
        $this->home = $home;
    }

    /**
     * @param Player $player
     */
    public function ranksForm(Player $player)
    {
        $form = $this->getHome()->getFormAPI()->createSimpleForm(function (Player $player, $data = null) {
            if ($data === null) {
                return;
            } else {
                $this->rankForm($player, $data);
            }
        });
        $formTitle = $this->getHome()->getProvider()->getMessage("form.main.title");
        $form->setTitle($formTitle);
        $ranks = $this->getHome()->getProvider()->getRanks();
        foreach ($ranks as $rank) {
            $text = $this->getHome()->getProvider()->getMessage("form.main.button");
            $text = str_replace("%rank%", $rank["name"], $text);
            if (isset($rank["formImg"]) && $rank["formImg"] != "") {
                $form->addButton($text, SimpleForm::IMAGE_TYPE_URL, $rank["formImg"]);
            } else {
                $form->addButton($text);
            }
        }
        $nickname = $player->getName();
        $text = $this->getHome()->getProvider()->getMessage("form.main.content");
        $rank = $this->getHome()->getProvider()->getRankPlayer($nickname);
        $rankName = $this->getHome()->getProvider()->getNameRank($rank);
        $level = $this->getHome()->getProvider()->getLevelPlayer($nickname);
        $exp = $this->getHome()->getProvider()->getExperiencePlayer($nickname);
        $text = str_replace("%rank%", $rankName, $text);
        $text = str_replace("%level%", $level, $text);
        $text = str_replace("%exp%", $exp, $text);
        if ($this->getHome()->getProvider()->isNextLevel($rank, $level)) {
            $last_exp = $this->getHome()->getProvider()->getLevelExp($rank, $level + 1);
        } else {
            $last_exp = $this->getHome()->getProvider()->getLevelExp($rank, $level);
        }
        $text = str_replace("%last_exp%", $last_exp, $text);
        $form->setContent($text);
        $form->sendToPlayer($player);
    }

    public function rankForm(Player $player, string $rank)
    {
        $form = $this->getHome()->getFormAPI()->createSimpleForm(function (Player $player, $data = null) {
            if ($data === null) {
                return;
            } else {
                $this->ranksForm($player);
            }
        });
        $rank = $this->getHome()->getProvider()->getRanks()[$rank];
        $text = $this->getHome()->getProvider()->getMessage("form.rank.title");
        $rankName = $rank["name"];
        $text = str_replace("%rank%", $rankName, $text);
        $form->setTitle($text);
        $text = $this->getHome()->getProvider()->getMessage("form.rank.content");
        $text = str_replace("%rank%", $rankName, $text);
        if (strpos($text, '%levels%') !== false) {
            $levels = $rank["levels"];
            $msg = null;
            $i = 0;
            foreach ($levels as $level) {
                $txt = $this->getHome()->getProvider()->getMessage("form.rank.level");
                $txt = str_replace("%level%", $i, $txt);
                $exp = $level;
                $txt = str_replace("%exp%", $exp, $txt);
                $msg .= "$txt\n";
                $i = $i + 1;
            }
            $text = str_replace("%levels%", $msg, $text);
        }
        $text = str_replace("%money%", $rank["gifts"]["money"], $text);
        $text = str_replace("%group%", $rank["gifts"]["group"], $text);
        $text = str_replace("%info%", $rank["info"], $text);
        $form->setContent($text);
        $text = $this->getHome()->getProvider()->getMessage("form.rank.button");
        $form->addButton($text);
        $form->sendToPlayer($player);
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }
}
