<?php

namespace ru\universalcrew\ranksystem\utils;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\utils\Config;
use ru\universalcrew\ranksystem\Home;

class Provider
{

    /**
     * @var Home
     */
    private $home;

    /**
     * @var Config
     */
    private $rankConfig;

    /**
     * @var Config
     */
    private $messagesConfig;

    /**
     * @var Config
     */
    private $usersConfig;

    /**
     * @var Config
     */
    private $experienceConfig;

    /**
     * @var array
     */
    private $users;

    /**
     * @var array
     */
    private $rank;

    /**
     * @var array
     */
    private $messages;

    /**
     * @var array
     */
    private $experience;

    /**
     * Provider constructor.
     * @param Home $home
     */
    public function __construct(Home $home)
    {
        $this->home = $home;
        @mkdir($this->getHome()->getDataFolder());
        if (!is_file($this->getHome()->getDataFolder() . 'rank.json')) {
            $this->getHome()->saveResource('rank.json');
        }
        if (!is_file($this->getHome()->getDataFolder() . 'messages.json')) {
            $this->getHome()->saveResource('messages.json');
        }
        if (!is_file($this->getHome()->getDataFolder() . 'experience.json')) {
            $this->getHome()->saveResource('experience.json');
        }
        $this->messagesConfig   = new Config($this->getHome()->getDataFolder() . 'messages.json');
        $this->messages         = $this->messagesConfig->getAll();
        $this->experienceConfig = new Config($this->getHome()->getDataFolder() . "experience.json");
        $this->experience       = $this->experienceConfig->getAll();
        $this->usersConfig      = new Config($this->getHome()->getDataFolder() . "users.json");
        $this->users            = $this->usersConfig->getAll();
        $this->rankConfig       = new Config($this->getHome()->getDataFolder() . 'rank.json');
        $this->rank             = $this->rankConfig->getAll();
    }

    /**
     * @param string $nickname
     * @return bool
     */
    public function isUser(string $nickname) : bool
    {
        $nickname = strtolower($nickname);
        return isset($this->users[$nickname]);
    }

    /**
     * @param string $nickname
     */
    public function createUser(string $nickname) : void
    {
        $nickname               = strtolower($nickname);
        $this->users[$nickname] = [
            "experience" => 0,
            "rank" => 0,
            "level" => 0
        ];
    }

    /**
     * @param string $nickname
     * @param int $exp
     */
    public function addExperience(string $nickname, int $exp) : void
    {
        $nickname   = strtolower($nickname);
        $currentExp = $this->users[$nickname]["experience"];
        if ($currentExp == 0) {
            $this->users[$nickname]["experience"] = $exp;
        } else {
            $this->users[$nickname]["experience"] = $currentExp + $exp;
        }
    }

    /**
     * @param string $nickname
     * @param int $exp
     */
    public function setExperience(string $nickname, int $exp) : void
    {
        $nickname                             = strtolower($nickname);
        $this->users[$nickname]["experience"] = $exp;
    }

    /**
     * @param string $nickname
     * @return int
     */
    public function getExperiencePlayer(string $nickname) : int
    {
        $nickname = strtolower($nickname);
        return $this->users[$nickname]["experience"];
    }

    /**
     * @param string $nickname
     * @return int
     */
    public function getRankPlayer(string $nickname) : int
    {
        $nickname = strtolower($nickname);
        return $this->users[$nickname]["rank"];
    }

    /**
     * @param string $nickname
     * @param int $rank
     */
    public function setRankPlayer(string $nickname, int $rank) : void
    {
        $nickname = strtolower($nickname);
        $this->users[$nickname]["rank"] = $rank;
    }

    /**
     * @param string $nickname
     * @return int
     */
    public function getLevelPlayer(string $nickname) : int
    {
        $nickname = strtolower($nickname);
        return $this->users[$nickname]["level"];
    }

    /**
     * @param string $nickname
     * @param int $level
     */
    public function setLevelPlayer(string $nickname, int $level) : void
    {
        $nickname = strtolower($nickname);
        $this->users[$nickname]["level"] = $level;
    }

    /**
     * @param int $rank
     * @param int $level
     * @return bool
     */
    public function isNextLevel(int $rank, int $level) : bool
    {
        $level = $level + 1;
        return isset($this->rank[$rank]["levels"][$level]);
    }

    /**
     * @param int $rank
     * @return bool
     */
    public function isRank(int $rank) : bool
    {
        return isset($this->rank[$rank]);
    }


    /**
     * @param int $rank
     * @return bool
     */
    public function isNextRank(int $rank) : bool
    {
        $rank = $rank + 1;
        return isset($this->rank[$rank]);
    }

    /**
     * @param int $rank
     * @param int $level
     * @return int
     */
    public function getLevelExp(int $rank, int $level) : int
    {
        return $this->rank[$rank]["levels"][$level];
    }

    /**
     * @param int $rank
     * @return string
     */
    public function getGiftGroup(int $rank) : string
    {
        return $this->rank[$rank]["gifts"]["group"];
    }

    /**
     * @param int $rank
     * @return int
     */
    public function getGiftMoney(int $rank) : int
    {
        return $this->rank[$rank]["gifts"]["money"];
    }

    /**
     * @param string $type
     * @param string $data
     * @return bool
     */
    public function isExperience(string $type, string $data) : bool
    {
        return isset($this->experience[$type][$data]);
    }

    /**
     * @param string $type
     * @param string $data
     * @return int
     */
    public function getExperience(string $type, string $data) : int
    {
        return $this->experience[$type][$data];
    }

    /**
     * @param int $rank
     * @return string
     */
    public function getNameRank(int $rank) : string
    {
        return $this->rank[$rank]["name"];
    }

    /**
     * @return array
     */
    public function getRanks() : array
    {
        return $this->rank;
    }

    /**
     * @param string $message
     * @return string
     */
    public function getMessage(string $message) : string
    {
        return $this->messages[$message];
    }

    public function save() : void
    {
        $this->usersConfig->setAll($this->users);
        $this->usersConfig->save();
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }
}
