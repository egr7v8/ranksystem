<?php

namespace ru\universalcrew\ranksystem\tasks;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\scheduler\Task;
use ru\universalcrew\ranksystem\utils\HotBar;

class HotBarTask extends Task
{

    private $hotBar;

    /**
     * HotBarTask constructor.
     * @param HotBar $hotBar
     */
    function __construct(HotBar $hotBar)
    {
        $this->hotBar = $hotBar;
    }

    /**
     * @param int $currentTick
     */
    function onRun($currentTick)
    {
        $this->getHotBar()->sendHotBar();
    }

    /**
     * @return HotBar
     */
    private function getHotBar() : HotBar
    {
        return $this->hotBar;
    }
}