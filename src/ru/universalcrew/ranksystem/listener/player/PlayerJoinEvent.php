<?php

namespace ru\universalcrew\ranksystem\listener\player;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerPreLoginEvent as Event;
use ru\universalcrew\ranksystem\Home;

class PlayerJoinEvent implements Listener
{

    /**
     * @var Home
     */
    private $home;


    /**
     * PlayerJoinEvent constructor.
     * @param Home $home
     */
    function __construct(Home $home)
    {
        $this->home = $home;
    }

    /**
     * @param Event $event
     * @priority HIGH
     */
    function onCall(Event $event)
    {
        $player = $event->getPlayer();
        $nickname = $player->getName();
        if (!$this->getHome()->getProvider()->isUser($nickname)) $this->getHome()->getProvider()->createUser($nickname);
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }

}