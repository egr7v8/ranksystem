<?php

namespace ru\universalcrew\ranksystem\listener\player;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\event\Listener;
use pocketmine\event\inventory\CraftItemEvent as Event;
use pocketmine\Player;
use ru\universalcrew\ranksystem\Home;

class PlayerCraftItemEvent implements Listener
{

    /**
     * @var Home
     */
    private $home;

    /**
     * PlayerDeathEvent constructor.
     * @param Home $home
     */
    function __construct(Home $home)
    {
        $this->home = $home;
    }

    /**
     * @param Event $event
     */
    function onCall(Event $event)
    {
        if ($event->isCancelled()) return;
        $player = $event->getPlayer();
        $recipe = $event->getRecipe();
        $item = $recipe->getResult();
        $id = $item->getId();
        if ($this->getHome()->getProvider()->isExperience("craft", $id)) $exp = $this->getHome()->getProvider()->getExperience("craft", $id);
        else $exp = $this->getHome()->getProvider()->getExperience("craft", "last");
        $this->getHome()->getExperience()->addExp($player, $exp);
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }

}