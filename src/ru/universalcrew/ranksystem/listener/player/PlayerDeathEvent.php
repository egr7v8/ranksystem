<?php

namespace ru\universalcrew\ranksystem\listener\player;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerDeathEvent as Event;
use pocketmine\Player;
use ru\universalcrew\ranksystem\Home;

class PlayerDeathEvent implements Listener
{

    /**
     * @var Home
     */
    private $home;

    /**
     * PlayerDeathEvent constructor.
     * @param Home $home
     */
    function __construct(Home $home)
    {
        $this->home = $home;
    }

    /**
     * @param Event $event
     */
    function onCall(Event $event)
    {
        $player = $event->getPlayer();
        $cause = $player->getLastDamageCause();
        if($cause instanceof EntityDamageByEntityEvent && $player instanceof Player){
            $killer = $cause->getDamager();
            $exp = $this->getHome()->getProvider()->getExperience("kill", "player");
            if ($killer instanceof Player) $this->getHome()->getExperience()->addExp($killer, $exp);
        }
    }

    /**
     * @return Home
     */
    private function getHome() : Home
    {
        return $this->home;
    }

}