<?php

namespace ru\universalcrew\ranksystem\commands;

/**
 *  _    _       _                          _  ____
 * | |  | |_ __ (_)_    _____ _ ______ __ _| |/ ___\_ _______      __
 * | |  | | '_ \| | \  / / _ \ '_/ __// _' | / /   | '_/ _ \ \    / /
 * | |__| | | | | |\ \/ /  __/ | \__ \ (_) | \ \___| ||  __/\ \/\/ /
 *  \____/|_| |_|_| \__/ \___|_| /___/\__,_|_|\____/_| \___/ \_/\_/
 *
 * @author egr7v8
 * @link   https://t.me/egr7v8
 *
 */

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\PluginIdentifiableCommand;
use pocketmine\Player;
use pocketmine\plugin\Plugin;
use ru\universalcrew\ranksystem\Home;

class SetRank extends Command implements PluginIdentifiableCommand
{

    /**
     * @var Home
     */
    private $home;

    /**
     * SetRank constructor.
     * @param Home $home
     */
    function __construct(Home $home)
    {
        parent::__construct("setrank", "Выдать ранг", "setrank <игрок> <номер ранга>", []);
        $this->setPermission('ru.universalcrew.ranksystem.setrank');
        $this->home = $home;
    }

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    function execute(CommandSender $sender, string $commandLabel, array $args)
    {
        if( !$this->testPermissionSilent($sender) )
        {
            $text = $this->getHome()->getProvider()->getMessage("no_permission");
            $sender->sendMessage($text);
            return;
        }
        if (count($args) > 1){
            $nickname = $args[0];
            $rank = $args[1];
            if (!$this->getHome()->getProvider()->isUser($nickname)){
                $text = $this->getHome()->getProvider()->getMessage("no_player");
                $sender->sendMessage($text);
                return;
            }
            if (!$this->getHome()->getProvider()->isRank($rank))
            {
                $text = $this->getHome()->getProvider()->getMessage("no_rank");
                $sender->sendMessage($text);
                return;
            }
            $groupName = $this->getHome()->getProvider()->getGiftGroup($rank);
            $player = $this->getHome()->getServer()->getPlayer($nickname);
            $group = $this->getHome()->getPurePerms()->getGroup($groupName);
            $this->getHome()->getPurePerms()->setGroup($player, $group);
            $this->getHome()->getProvider()->setRankPlayer($nickname, $rank);
            $this->getHome()->getProvider()->setExperience($nickname, 0);
            $text = $this->getHome()->getProvider()->getMessage("setrank");
            $text = str_replace("%nickname%", $nickname, $text);
            $text = str_replace("%rank%", $rank, $text);
            $sender->sendMessage($text);
        } else $sender->sendMessage($this->usageMessage);
        return;
    }

    function getHome() : Home
    {
        return $this->home;
    }

    /**
     * @return Plugin
     */
    function getPlugin() : Plugin
    {
        return $this->home;
    }

}
